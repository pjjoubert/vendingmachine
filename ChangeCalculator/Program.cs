﻿using System;
using System.Collections.Generic;

namespace ChangeCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var denominations = new int[] { 50, 25, 20, 10, 5, 2, 1};
            var machine = new VendingMachine(denominations);
            var coins = machine.CalculateChange(0.83M, 1.25M);

            if (coins.Length > 0)
            {
                foreach (var key in coins)
                {
                    Console.WriteLine($"{key} ");
                }
            }
            else
            {
                Console.WriteLine("No coins returned.");
            }

            Console.ReadLine();
        }
    }
}
