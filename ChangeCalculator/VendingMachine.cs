﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChangeCalculator
{
    public class VendingMachine : IVendingMachine
    {
        private readonly int[] coinTypes;        

        public VendingMachine(int[] coinDenominations)
        {
            coinTypes = coinDenominations;
            Array.Sort<int>(coinTypes);
            Array.Reverse(coinTypes);
        }

        public int[] CalculateChange(decimal purchaseAmount, decimal tenderAmount)
        {
            var changeAmount = tenderAmount - purchaseAmount;

            return CalculateCoins(decimal.ToInt32(changeAmount * 100)); //change to cents.
        }

        public int[] CalculateCoins(int changeAmount)
        {
            var coinAmounts = new List<int>();

            if (changeAmount > 0)
            {
                foreach(var coin in coinTypes)
                {
                    while ( (changeAmount - coin) >= coin || (changeAmount - coin) >= 0)
                    {
                        coinAmounts.Add(coin);
                        changeAmount = changeAmount - coin;
                    }
                }
            }

            return coinAmounts.ToArray();
        }

    }
}
