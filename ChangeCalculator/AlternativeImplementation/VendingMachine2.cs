﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChangeCalculator
{
    public class VendingMachine2 : IVendingMachine2
    {
        private readonly int[] coinTypes;
        private Dictionary<int, int> coinAmounts;

        public VendingMachine2(int[] coinDenominations)
        {
            coinTypes = coinDenominations;
            Array.Sort<int>(coinTypes);
            Array.Reverse(coinTypes); //ensure cointypes are in descending order

            coinAmounts = new Dictionary<int, int>();
            foreach (var key in coinDenominations)
            {
                coinAmounts.Add(key, 0);
            }
        }

        public Dictionary<int, int> CalculateChange(double purchaseAmount, double tenderAmount)
        {
            var changeAmount = tenderAmount - purchaseAmount;

            return CalculateCoins(Convert.ToInt32(changeAmount * 100)); //change to cents.
        }

        public Dictionary<int, int> CalculateCoins(int changeAmount)
        {
            if (changeAmount > 0)
            {
                foreach (var key in coinTypes)
                {
                    coinAmounts[key] = changeAmount / key; //calc the number of this type of coin.
                    changeAmount = changeAmount % key; //adjust the remaining amount for the next coin type
                }
            }

            return coinAmounts;
        }
    }
}
