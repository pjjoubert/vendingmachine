﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChangeCalculator
{
    public interface IVendingMachine2
    {
       Dictionary<int, int> CalculateChange(double purchaseAmount, double tenderAmount);
       Dictionary<int, int> CalculateCoins(int changeAmount);
    }
}
