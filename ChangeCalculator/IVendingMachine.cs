﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChangeCalculator
{
    public interface IVendingMachine
    {
        int[] CalculateChange(decimal purchaseAmount, decimal tenderAmount);
        int[] CalculateCoins(int changeAmount);
    }
}
