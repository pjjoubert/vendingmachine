using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChangeCalculator;
using System.Collections.Generic;
using FluentAssertions;

namespace VendingMachineTests
{
    [TestClass]
    public class VendingMachineTests
    {
        [TestMethod]
        public void CalculateChange_Should_NotReturn_Null()
        {
            //setup
            var denominations = new int[] { 50, 25, 20, 10, 5, 2, 1 };
            var machine = new VendingMachine(denominations);

            //test
            var coins = machine.CalculateChange(0.83M, 1.25M);

            //assert
            coins.Should().NotBeNull();
            coins.Should().BeOfType<int[]>();

        }

        [TestMethod]
        public void CalculateChange_Should_Return_Array()
        {
            //setup
            var denominations = new int[] { 50, 25, 20, 10, 5, 2, 1 };
            var machine = new VendingMachine(denominations);

            //test
            var coins = machine.CalculateChange(0.83M, 1.25M);

            //assert
            coins.Should().NotBeNull();
            coins.Should().BeOfType(typeof(int[]));
        }
    }
}
